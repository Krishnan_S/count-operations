const Express = require("express");
const app = Express();
let count = 10;

app.use(Express.json());
app.get("/count", (req, res) => {
  res.send(JSON.stringify(count));
});
app.get("/count/incress", (req, res) => {
  count += 1;
  res.send(JSON.stringify(count));
});
app.get("/count/decress", (req, res) => {
  count -= 1;
  res.send(JSON.stringify(count));
});

const Port = 4000;
const Port2 = 4001;
app.listen(Port || Port2, () => console.log("Listening to port: ", Port));
